# ORKG Exploration Chatbot

This is a chatbot for the ORKG (Open Research Knowledge Graph) that allows users to explore the ORKG and its content. The chatbot is based on the [DialogFlow](https://dialogflow.com) platform.

This project started as a small student collaboration at HTWK Leipzig in the context of the [ORKG](https://orkg.org) project.

## The OG group
Three awesome students from the HTWK Leipzig, Germany: Joana Zapp, Lukas Weigel, and Maja Hoffmann.

With the sage advice of ORKG's [Hassan](https://gitlab.com/webo1980) and [Yaser](https://gitlab.com/YaserJaradeh) 🤠

## Code
An archive of the artifacts generated during the work is part of this repo

## Sneak Peek
Say hi to Suzie

![Example screencast of how Suzie works](/img/Suzie.gif)
